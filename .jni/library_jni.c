#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "jni_inout.h"
#include "library.c"
extern JNIEnv *javaEnv;

/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

JNIEXPORT jobjectArray JNICALL Java_library_minorMatrix_1
  (JNIEnv *env, jobject object, jint deleteRow, jint deleteColumn, jobjectArray matrix, jint matrixRows, jint matrixColumns)
{
    javaEnv = env;
    int c_deleteRow = toInt(deleteRow);
    int c_deleteColumn = toInt(deleteColumn);
    int c_matrixRows = toInt(matrixRows);
    int c_matrixColumns = toInt(matrixColumns);
    double** c_matrix = toDoubleMatrix(matrix, c_matrixRows, c_matrixColumns);
    int c_minorMatrixRows = c_matrixRows-1;
    int c_minorMatrixColumns = c_matrixColumns-1;
    double** c_outValue = minorMatrix(c_deleteRow, c_deleteColumn, c_matrix, c_matrixRows, c_matrixColumns);
    setJdoubleMatrix(matrix , c_matrix, &c_matrixRows, &c_matrixColumns);
    if (c_outValue == NULL) {
        return NULL;
    } else {    
        return toJdoubleMatrix(c_outValue, &c_minorMatrixRows, &c_minorMatrixColumns);
    }
}

JNIEXPORT jdouble JNICALL Java_library_determinant_1
  (JNIEnv *env, jobject object, jobjectArray matrix, jint matrixRows, jint matrixColumns)
{
    javaEnv = env;
    int c_matrixRows = toInt(matrixRows);
    int c_matrixColumns = toInt(matrixColumns);
    double** c_matrix = toDoubleMatrix(matrix, c_matrixRows, c_matrixColumns);
    double c_outValue = determinant(c_matrix, c_matrixRows, c_matrixColumns);
    setJdoubleMatrix(matrix , c_matrix, &c_matrixRows, &c_matrixColumns);
    return toJdouble(c_outValue);
}

JNIEXPORT jobjectArray JNICALL Java_library_matrixWithReplacedColumn_1
  (JNIEnv *env, jobject object, jint columnIndex, jdoubleArray columnValue, jobjectArray matrix, jint matrixRows, jint matrixColumns)
{
    javaEnv = env;
    int c_columnIndex = toInt(columnIndex);
    int c_columnValueLength = toInt(matrixRows);
    double* c_columnValue = toDoubleArray(columnValue, c_columnValueLength);
    int c_matrixRows = toInt(matrixRows);
    int c_matrixColumns = toInt(matrixColumns);
    double** c_matrix = toDoubleMatrix(matrix, c_matrixRows, c_matrixColumns);
    int c_matrixWithReplacedColumnRows = c_matrixRows;
    int c_matrixWithReplacedColumnColumns = c_matrixColumns;
    double** c_outValue = matrixWithReplacedColumn(c_columnIndex, c_columnValue, c_matrix, c_matrixRows, c_matrixColumns);
    setJdoubleArray(columnValue , c_columnValue, &c_columnValueLength);
    setJdoubleMatrix(matrix , c_matrix, &c_matrixRows, &c_matrixColumns);
    if (c_outValue == NULL) {
        return NULL;
    } else {    
        return toJdoubleMatrix(c_outValue, &c_matrixWithReplacedColumnRows, &c_matrixWithReplacedColumnColumns);
    }
}

JNIEXPORT jdoubleArray JNICALL Java_library_cramerSystemUnknown_1
  (JNIEnv *env, jobject object, jobjectArray coeficient, jint coeficientRows, jint coeficientColumns, jdoubleArray independent)
{
    javaEnv = env;
    int c_coeficientRows = toInt(coeficientRows);
    int c_coeficientColumns = toInt(coeficientColumns);
    double** c_coeficient = toDoubleMatrix(coeficient, c_coeficientRows, c_coeficientColumns);
    int c_independentLength = toInt(coeficientRows);
    double* c_independent = toDoubleArray(independent, c_independentLength);
    int c_cramerSystemUnknownLength = c_independentLength;
    double* c_outValue = cramerSystemUnknown(c_coeficient, c_coeficientRows, c_coeficientColumns, c_independent);
    setJdoubleMatrix(coeficient , c_coeficient, &c_coeficientRows, &c_coeficientColumns);
    setJdoubleArray(independent , c_independent, &c_independentLength);
    if (c_outValue == NULL) {
        return NULL;
    } else {    
        return toJdoubleArray(c_outValue, &c_cramerSystemUnknownLength);
    }
}
